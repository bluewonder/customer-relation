<?php
/**
 * Wishlattedesk_Customerlistreport
 *
 * @category    Wishlattedesk
 * @package     Wishlattedesk_Customerlistreport
 * @copyright   Copyright (c) 2014 Wishlattedesk Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author      Hieu Nguyen (Wishlattedesk's team)
 * @email       bzaikia@gmail.com
 */

class Wishlattedesk_Customerlistreport_Block_Adminhtml_Report_Customer extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('customerGrid');
        $this->setUseAjax(true);
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $session = $this->_getSession();
        $filterData = $session->getData('customer_report_filter');
        if (!empty($filterData) && is_array($filterData)) {
            $collection = Mage::getResourceModel('customer/customer_collection')
                ->addNameToSelect()
                ->addAttributeToSelect('email')
                ->addAttributeToSelect('created_at')
                ->joinAttribute('billing_telephone', 'customer_address/telephone', 'default_billing', null, 'left');

            if (!empty($filterData['joinDateFrom'])) {
                $collection->addFieldToFilter('created_at', array('gt' => $filterData['joinDateFrom']));
            }

            if (!empty($filterData['joinDateTo'])) {
                $collection->addFieldToFilter('created_at', array('lt' => $filterData['joinDateTo']));
            }

            if (!empty($filterData['customerGroup'])) {
                $collection->addFieldToFilter('group_id', array('eq' => $filterData['customerGroup']));
            }

            if (!empty($filterData['productIds']) && $filterData['productFilter'] == 1 ) {
                $ids = $filterData['productIds'];
                if (strpos($ids, ',') !== false) {
                    $ids = explode(',', $ids);
                } else {
                    $ids = array($ids);
                }

                $helper = Mage::helper('customerlistreport/sql');
                $resource = Mage::getSingleton('core/resource');
                $zendDb = $helper->getZendDB();
                /** @var $select Zend_Db_Select*/
                $select = $zendDb->select();
                $select->from(
                        array('items' => $resource->getTableName('sales/order_item')), // table name
                        array('item_id', 'order_id', 'product_id') //  selected columns
                    )->where('product_id IN (?) ',$ids);

                $select->joinLeft(array('order' => $resource->getTableName('sales/order')), 'order.entity_id = items.order_id', 'order.customer_id');

                $collection->getSelect()->join(array('o' => new Zend_Db_Expr(sprintf('(%s)', $select))), 'e.entity_id = o.customer_id');
            }

        } else {
            $collection = Mage::getResourceModel('customer/customer_collection')->addAttributeToFilter('entity_id', array('eq' => 0));
        }
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('entity_id', array(
            'header'    => Mage::helper('customerlistreport')->__('ID'),
            'width'     => '50px',
            'index'     => 'entity_id',
            'type'  => 'number',
        ));

        $this->addColumn('name', array(
            'header'    => Mage::helper('customerlistreport')->__('Name'),
            'index'     => 'name'
        ));

        $this->addColumn('Telephone', array(
            'header'    => Mage::helper('customerlistreport')->__('Telephone'),
            'width'     => '100',
            'index'     => 'billing_telephone'
        ));



        $this->addExportType('*/*/exportExcel', Mage::helper('customer')->__('Excel'));
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/customergrid', array('_current'=> true));
    }

    public function getRowUrl($row)
    {
        return '';
    }

    public function getXlsFile()
    {
        $this->_prepareGrid();

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()
            ->setTitle($this->__('Customer Report'));

        $objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')
            ->setSize(10);
        // Column title
        $objPHPExcel->getActiveSheet()->setCellValue('A1', Mage::helper('customerlistreport')->__('ID'))
            ->setCellValue('B1', Mage::helper('customerlistreport')->__('Name'))
            ->setCellValue('C1', Mage::helper('customerlistreport')->__('Telephone'))
            ->setCellValue('D1', Mage::helper('customerlistreport')->__('Relations'));

        $i = 2;
        foreach ($this->getCollection() as $_index => $_item) {
            $j = 1;
            $arr = array(1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D');
            $customer = Mage::getModel('customer/customer');
            foreach ($this->getColumns() as $_column) {
                if ($_column->getIndex() == 'entity_id') {
                    $customer->load($_column->getRowField($_item));
                }
                if ($this->shouldRenderCell($_item, $_column)) {
                    $_html = $_column->getRowField($_item);
                    $objPHPExcel->getActiveSheet()->setCellValue($arr[$j].$i, $_html);
                    $j++;
                }
            }

            // Customer relationship data
            $relativeCollection = Mage::getModel('customerrelationship/customer_relation')->getCollection();
            $relativeCollection->addFieldToFilter('customer_id', array('eq' => $customer->getId()));
            $html = '';
            foreach($relativeCollection as $relative) {
                $relativeCustomer = Mage::getModel('customer/customer')->load($relative->getRelative());
                $relationship = Mage::getModel('customerrelationship/relationship')->load($relative->getRelationshipId());
                $html .= $relativeCustomer->getName() . ' - ' . $relationship->getTitle() . '(' . $relative->getRemark() . ')'."\r\n";
            }
            $objPHPExcel->getActiveSheet()->setCellValue($arr[$j].$i, $html);
            $i++;
        }


        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    /**
     * @return Mage_Adminhtml_Model_Session
    */
    protected function _getSession()
    {
        return Mage::getSingleton('adminhtml/session');
    }
}